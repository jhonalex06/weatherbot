using System;
using System.Threading.Tasks;
using Telegram.Bot;
using System.Threading;
using Telegram.Bot.Extensions.Polling;
using Telegram.Bot.Exceptions;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using TelegramBotweather;
using Telegram.Bot.Types.ReplyMarkups;

namespace TelegramBotHandlers{
public class Handlers
{
    public static Task HandleErrorAsync(ITelegramBotClient botClient, Exception exception, CancellationToken cancellationToken)
            {
                var ErrorMessage = exception switch
                {
                    ApiRequestException apiRequestException => $"Telegram API Error:\n[{apiRequestException.ErrorCode}]\n{apiRequestException.Message}",
                    _                                       => exception.ToString()
                };

                Console.WriteLine(ErrorMessage);
                return Task.CompletedTask;
            }

    public static async Task HandleUpdateAsync(ITelegramBotClient botClient, Update update, CancellationToken cancellationToken)
    {
        if (update.Type != UpdateType.Message)
            return;
        // if (update.Message.Type != MessageType.Text)
        //     return;

        bool isText = true;
        string[] weatherdata; 
        string weatherstatus = "";
        string airstatus = "";
        string weatherpicstatus = "01n";

        if (update.Message.Type == MessageType.Location ){
            string lat = update.Message.Location.Latitude.ToString();
            string log = update.Message.Location.Longitude.ToString();

            weatherdata = weather.weatherStatus(log, lat);
            weatherstatus = weatherdata[0];
            weatherpicstatus = weatherdata[1];
            airstatus = weather.AirStatus(log, lat);
            isText = false;
        }

        var chatId = update.Message.Chat.Id;
        
        Console.WriteLine($"Received a '{update.Message.Text}' message in chat {chatId}.");

        KeyboardButton[] array1  = new KeyboardButton[] { KeyboardButton.WithRequestLocation("Share Location")};
        ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup(array1);


        if (isText) {
            await botClient.SendTextMessageAsync(
            chatId: chatId,
            text:   "You said:\n" + update.Message.Text + "\n" + weatherstatus + "\n" + airstatus,
            replyMarkup: replyKeyboardMarkup
            );
        } else {
            await botClient.SendPhotoAsync(
            chatId: chatId,
            photo: String.Format("https://openweathermap.org/img/wn/{0}@2x.png", weatherpicstatus),
            caption: weatherstatus + "\n" + airstatus,
            parseMode: ParseMode.Html,
            cancellationToken: cancellationToken);
        }
        }
}
}