using System;
using System.IO;
using System.Net;

using Newtonsoft.Json;
using TelegramBotpasswords;

namespace TelegramBotweather{
public class weather
{
    public static string[] weatherStatus(string log, string lat){
            string weatherstatus = "";
            string _weatherapi = passwords.GetPassword("WeatherApiKey");

            string html = string.Empty;
            string initialurl = @"https://api.openweathermap.org/data/2.5/weather?lat={0}&lon={1}&appid={2}&units=metric";

            string url = String.Format(initialurl, lat, log, _weatherapi);

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.AutomaticDecompression = DecompressionMethods.GZip;

            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            using (Stream stream = response.GetResponseStream())
            using (StreamReader reader = new StreamReader(stream))
            {
                html = reader.ReadToEnd();
            }
            dynamic json = JsonConvert.DeserializeObject(html);
            
            // Console.WriteLine(json["weather"][0]["description"]);
            // Console.WriteLine(json["main"]["temp"]);

            string description = json["weather"][0]["description"];
            string temp = json["main"]["temp"];
            string weatherpicstatus = json["weather"][0]["icon"];

            weatherstatus = String.Format("Description: \t {0} \nTemp: \t {1} Celsius", description, temp);

            return new string[] {weatherstatus, weatherpicstatus};
    }

    public static string AirStatus(string log, string lat){
            string airstatus = "";
            string _weatherapi = passwords.GetPassword("WeatherApiKey");

            string html = string.Empty;
            string initialurl = @"https://api.openweathermap.org/data/2.5/air_pollution?lat={0}&lon={1}&appid={2}&units=metric";

            string url = String.Format(initialurl, lat, log, _weatherapi);

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.AutomaticDecompression = DecompressionMethods.GZip;

            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            using (Stream stream = response.GetResponseStream())
            using (StreamReader reader = new StreamReader(stream))
            {
                html = reader.ReadToEnd();
            }
            dynamic json = JsonConvert.DeserializeObject(html);

            string quality = json["list"][0]["main"]["aqi"];
            string pm10 = json["list"][0]["components"]["pm10"];

            airstatus = String.Format("Air Quality Level: \t {0} \nPm 10: \t {1} μg/m3", quality, pm10);

            return airstatus;
    }
}
}