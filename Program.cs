﻿using System;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using System.Threading;

using Newtonsoft.Json;
using Telegram.Bot;
using Telegram.Bot.Extensions.Polling;
using TelegramBotHandlers;
using TelegramBotpasswords;

namespace TelegramBot
{
    class Program
    {
        static async Task Main(string[] args)
        {
            string _telegrambotapi = passwords.GetPassword("TelegramBotApiKey");
            
            var botClient = new TelegramBotClient(_telegrambotapi);
            var me = await botClient.GetMeAsync();
            Console.WriteLine($"Hello, World! I am user {me.Id} and my name is {me.FirstName}.");

            using var cts = new CancellationTokenSource();

            // StartReceiving does not block the caller thread. Receiving is done on the ThreadPool.
            botClient.StartReceiving(new DefaultUpdateHandler(Handlers.HandleUpdateAsync, Handlers.HandleErrorAsync), cts.Token);

            Console.WriteLine($"Start listening for @{me.Username}");
            Console.ReadLine();

            // Send cancellation request to stop bot
            cts.Cancel();
        } 
    }
}
