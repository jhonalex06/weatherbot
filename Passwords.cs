using System;
using System.IO;
using System.Net;
using Newtonsoft.Json;

namespace TelegramBotpasswords{
public class passwords
{
    public static string GetPassword(string key){
            string secrets = string.Empty;
            using (StreamReader reader = new StreamReader("secrets.json"))
            {
                secrets = reader.ReadToEnd();
            }
            dynamic json = JsonConvert.DeserializeObject(secrets);

            return String.Format("{0}", json[key]);
        }
}
}